import React, {useState, useEffect, useRef, useCallback} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import { useForm } from "react-hook-form";

import lending from './lending.png';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

// Templates and styles for material design
// Source: https://material-ui.com/
const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function LendingForm() {
  const classes = useStyles();

  // Button state
  const [disabled, setDisabled] = useState(false)

  // Snackbar state
  const [open, setOpen] = useState(false)

  // Values coming from the API
  const [verdict,setVerdict] = useState("")
  const [severity,setSeverity] = useState("success")

  // Form values
  const [taxId, setTaxId] = useState("")
  const [businessName, setBusinessName] = useState("")
  const [amount, setAmount] = useState("")

  // Calling the api based on values
  const fetchData = useCallback(()=>{
    setDisabled(true)
    setOpen(false)
    fetch("https://lendingback.me/ws/apply/"+taxId+"/"+businessName+"/"+amount, {
    "method": "GET",
    "headers": {
        "access-control-allow-origin": "*"
    }
    })
    .then(response => {
        //console.log(response.json());
        return response.json()
        
    }).then(data => {
        console.log(data);
        setVerdict(data.verdict)
        setSeverity(data.severity)
        setDisabled(false)
        setOpen(true)
    }).catch(err => {
        console.error(err);
        setVerdict("There was a horrible error with the server or the network connection. Please try again")
        setSeverity("error")
        setDisabled(false)
        setOpen(true)
    });
  });

  // Functions for handling changes in forms  

  function onChangeTaxId(value) {
    setTaxId(value)
  }

  function onChangeBusinessName(value) {
    setBusinessName(value)    
  }

  function onChangeAmount(value) {
    setAmount(value)
  }

  // Handle snackbar closure
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };


  return (
    // The following is the material design inspired template
    <Container component="main" maxWidth="xs">
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert severity={severity}>
          {verdict}
        </Alert>
      </Snackbar>
      <CssBaseline />
      <div className={classes.paper}>
        <img src={lending} width={400}/>
        <form className={classes.form} validate>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                autoComplete="fname"
                name="taxId"
                variant="outlined"
                required
                fullWidth
                id="taxId"
                label="Tax ID"
                value={taxId}
                autoFocus
                onChange={e => onChangeTaxId(e.target.value)}                
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="businessName"
                label="Business name"
                name="businessName"
                value={businessName}
                onChange={e => onChangeBusinessName(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
                <FormControl fullWidth className={classes.margin} variant="outlined">
                    <InputLabel htmlFor="requestedAmount">Requested amount</InputLabel>
                    <OutlinedInput
                        id="requestedAmount"
                        required
                        //value={values.amount}
                        //onChange={handleChange('amount')}
                        startAdornment={<InputAdornment position="start">$</InputAdornment>}
                        labelWidth={140}
                        value={amount}
                        onChange={e => onChangeAmount(e.target.value)}
                    />
                </FormControl>
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={fetchData}
            disabled={disabled || (taxId.length < 1 || businessName.length < 1 || amount.length < 1)}
          >
            Apply!
          </Button>
        </form>
      </div>
    </Container>
  );
}