import logo from './logo.svg';
import './App.css';

import React from 'react';
import Header from './components/Header';
import LendingForm from './components/LendingForm';
import { Button, Container, Input } from '@material-ui/core';

function App() {
  return (
    <div className="container">
      <LendingForm />     
    </div>
  );
}

export default App;
